﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Api_Intigartion.Models.SAPInventory;

namespace Api_Intigartion.Models.Repository
{
    public class SAPInventory
    {
        ApiIntegration_dbEntities db = new ApiIntegration_dbEntities();
        public List<IF_Inventory> AddUpdateSAPInventory(List<Result> inventories, IF_Inventory inventory)
        {

            try
            {
                if (inventories.Count != 0)
                {
                    foreach (var item in inventories)
                    {
                        var exist = db.IF_Inventory.Any(x => x.Tenant_Id == item.PhysicalInventoryDocument) ? true : false;
                        if (!exist)
                        {
                            inventory.Tenant_Id = item.PhysicalInventoryDocument;
                            inventory.Location = item.StorageLocation;
                            inventory.FulFillment_Center_Id = item.Plant;
                            inventory.Company_Id = item.PhysInvtryRecountDocument;
                            inventory.Product = item.Material;
                            inventory.Quantity = item.Quantity;
                            inventory.Area = item.UnitOfEntry;
                            db.IF_Inventory.Add(inventory);
                            db.SaveChanges();
                        }

                    }
                }

            }
            catch (Exception e)
            {

            }
            return db.IF_Inventory.ToList();
            //return View(inventroy_list);

        }
    }
}