﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api_Intigartion.Models
{
    public class InventoryViewModel
    {
      
        public string FiscalYear { get; set; }
        public string PhysicalInventoryDocument { get; set; }
        public string PhysicalInventoryDocumentItem { get; set; }
        public string Plant { get; set; }
        public string StorageLocation { get; set; }
        public string Material { get; set; }
        public string Batch { get; set; }
        public string InventorySpecialStockType { get; set; }
        public string PhysicalInventoryStockType { get; set; }
        public string SalesOrder { get; set; }
        public string SalesOrderItem { get; set; }
        public string Supplier { get; set; }
        public string Customer { get; set; }
        public string WBSElement { get; set; }
        public string LastChangeUser { get; set; }
        public DateTime? LastChangeDate { get; set; }
        public string CountedByUser { get; set; }
        public DateTime? PhysicalInventoryLastCountDate { get; set; }
        public string AdjustmentPostingMadeByUser { get; set; }
        public DateTime? PostingDate { get; set; }
        public bool PhysicalInventoryItemIsCounted { get; set; }
        public bool PhysInvtryDifferenceIsPosted { get; set; }
        public bool PhysInvtryItemIsRecounted { get; set; }
        public bool PhysInvtryItemIsDeleted { get; set; }
        public bool IsHandledInAltvUnitOfMsr { get; set; }
        public string CycleCountType { get; set; }
        public bool IsValueOnlyMaterial { get; set; }
        public string PhysInventoryReferenceNumber { get; set; }
        public string MaterialDocument { get; set; }
        public string MaterialDocumentYear { get; set; }
        public string MaterialDocumentItem { get; set; }
        public string PhysInvtryRecountDocument { get; set; }
        public bool PhysicalInventoryItemIsZero { get; set; }
        public string ReasonForPhysInvtryDifference { get; set; }
        public string MaterialBaseUnit { get; set; }
        public string BookQtyBfrCountInMatlBaseUnit { get; set; }
        public string Quantity { get; set; }
        public string UnitOfEntry { get; set; }
        public string QuantityInUnitOfEntry { get; set; }
        public string Currency { get; set; }
        public string DifferenceAmountInCoCodeCrcy { get; set; }
        public string EnteredSlsAmtInCoCodeCrcy { get; set; }
        public string SlsPriceAmountInCoCodeCrcy { get; set; }
        public string PhysInvtryCtAmtInCoCodeCrcy { get; set; }
        public string BookQtyAmountInCoCodeCrcy { get; set; }
        public DateTime LastChangeDateTime { get; set; }
    }
}