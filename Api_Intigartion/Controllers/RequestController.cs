﻿using Newtonsoft.Json;
using Sage_One_Authorisation_Client.RequestHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Api_Intigartion.Controllers
{
    public class RequestController : Controller
    {
        // GET: Request
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SageRequest(string endpoint, string method)
        {

            string country = Session["country"].ToString();
            endpoint = endpoint.ToLower();
            string baseurl = String.Format("https://api.accounting.sage.com/v3.1/{0}", endpoint);
            Uri endpointUri = new Uri(baseurl);

            string token = Session["token"].ToString();
            string siteID = Session["site_id"].ToString();
            RequestHelper request = new RequestHelper();
            string result = request.GetRequest(endpointUri, token, siteID);
            var obj = JsonConvert.DeserializeObject(result);
            var formatted = JsonConvert.SerializeObject(obj, Formatting.Indented);
            return Json(formatted, JsonRequestBehavior.AllowGet);
        }
    }
}