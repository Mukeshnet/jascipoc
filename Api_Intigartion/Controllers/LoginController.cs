﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Api_Intigartion.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        public ActionResult Login()
        {
            Session.Clear();
            return View();
        }

        [HttpPost]
        public ActionResult Login(Api_Intigartion.Models.Login login)
        {
            Api_Intigartion.Models.Repository.Login userObj = new Api_Intigartion.Models.Repository.Login();
            var userExits = userObj.ValidateUser(login.UserName ,login.Password);
            if (!userExits)
            {
                ViewBag.error = "Username and password doesn't match.";
                Session["UserName"] = null;
            }
            else
            {
                Session["UserName"] = "Valid";
                return RedirectToAction("ERPTypes", "Home");
            }
            return View();
        }
    }
}