﻿using Api_Intigartion.Models;
using Newtonsoft.Json;
using Sage_One_Authorisation_Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Api_Intigartion.Controllers
{
    public class HomeController : Controller
    {

        ApiIntegration_dbEntities db = new ApiIntegration_dbEntities();

        public ActionResult Index()
        {
            if (!IsValidUser())
            {
                return RedirectToAction("Login", "login");
            }
            return View();
        }




        public bool IsValidUser()
        {
            if (Session["UserName"] != null && Session["UserName"].ToString() != string.Empty)
            {
                return true;
            }
            return false;
        }

        [HttpGet]
        public ActionResult ERPTypes()
        {
            if (!IsValidUser())
                return RedirectToAction("Login", "login");
            return View();
        }


        [HttpGet]
        public ActionResult FetchERPData(string apiKey = "")
        {
            if (!IsValidUser())
                return RedirectToAction("Login", "login");
            if (apiKey != string.Empty)
                Session["APIKey"] = apiKey;
            if (Session["ERPNAME"].ToString() == "SAP")
                return Json(GetInventoryDataSAP(), JsonRequestBehavior.AllowGet);
            return Json("Please enter valid key.", JsonRequestBehavior.AllowGet);
        }
        public List<SAPInventory.Result> GetInventoryDataSAP()
        {
            List<SAPInventory.Result> invData = new List<SAPInventory.Result>();
            var url = ConfigurationManager.AppSettings["Inventory"];
            var apikey = ConfigurationManager.AppSettings["Inventory_apikey"];
            if (Session["APIKey"] != null && Session["APIKey"] != string.Empty)
                apikey = Session["APIKey"].ToString();
            var json = string.Empty;

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = "GET";
                request.Accept = "application/json";
                request.Headers["apikey"] = apikey;

                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

                var content = string.Empty;
                List<SAPInventory.Result> transformlist = new List<SAPInventory.Result>();

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            content = sr.ReadToEnd();
                            var result = JsonConvert.DeserializeObject<SAPInventory.RootObject>(content);
                            invData = result.d.results;
                            Session["Data"] = invData;
                        }
                    }
                }

            }
            catch (WebException exp)
            {
                throw exp;
            }

            return invData;

        }


        public static List<SAPInventory.Result> TransformationData(SAPInventory.Result data)
        {

            List<SAPInventory.Result> resultdata = new List<SAPInventory.Result>();
            resultdata.Add(new SAPInventory.Result { Customer = data.Customer, FiscalYear = data.FiscalYear, Currency = data.Currency, Plant = data.Plant, AdjustmentPostingMadeByUser = data.AdjustmentPostingMadeByUser, PhysicalInventoryDocument = data.PhysicalInventoryDocument });

            return resultdata;
        }




        [HttpGet]
        public ActionResult Material_stock_Data()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Material_stock_Data(IF_Inventory inventory)
        {
            var url = ConfigurationManager.AppSettings["Matarial_stock"];
            var apikey = ConfigurationManager.AppSettings["Inventory_apikey"];
            var json = string.Empty;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = "GET";
                request.Accept = "application/json";
                request.Headers["apikey"] = apikey;

                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

                var content = string.Empty;
                List<SAPInventory.Material_Result> transformlist = new List<SAPInventory.Material_Result>();

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            content = sr.ReadToEnd();
                            // json = new JavaScriptSerializer().Serialize(content);
                            var result = JsonConvert.DeserializeObject<SAPInventory.Material_RootObject>(content);

                            foreach (var item in result.d.results)
                            {
                                var exist = db.IF_Inventory.Any(x => x.Tenant_Id == item.Material) ? true : false;
                                if (!exist)
                                {
                                    //inventory.Tenant_Id = item.PhysicalInventoryDocument;
                                    //inventory.Location = item.StorageLocation;
                                    //inventory.FulFillment_Center_Id = item.Plant;
                                    //inventory.Company_Id = item.PhysInvtryRecountDocument;
                                    //inventory.Product = item.Quantity;
                                    //inventory.Quality = "Good";
                                    //inventory.Area = item.UnitOfEntry;
                                    //db.IF_Inventory.Add(inventory);
                                    //db.SaveChanges();
                                }
                                else
                                {
                                    //var inventory_data = db.IF_Inventory.Where(x => x.Tenant_Id == item.PhysicalInventoryDocument).FirstOrDefault();
                                    //inventory_data.Tenant_Id = item.PhysicalInventoryDocument;
                                    //inventory_data.Location = item.StorageLocation;
                                    //inventory_data.FulFillment_Center_Id = item.Plant;
                                    //inventory_data.Company_Id = item.PhysInvtryRecountDocument;
                                    //inventory_data.Product = item.Quantity;
                                    //inventory_data.Quality = "Good";
                                    //inventory_data.Area = item.UnitOfEntry;
                                    //inventory_data.Id = inventory_data.Id;
                                    //db.Entry(inventory).State = System.Data.Entity.EntityState.Modified;
                                }

                                transformlist = Material_TransformationData(item);
                            }
                            var transform_json = JsonConvert.SerializeObject(transformlist);
                            TempData["transform_json"] = transform_json;
                            TempData["ActualJson"] = content;
                        }
                    }
                }

            }
            catch (WebException exp)
            {

            }
            return RedirectToAction("Index");
        }
        public static List<SAPInventory.Material_Result> Material_TransformationData(SAPInventory.Material_Result data)
        {
            List<SAPInventory.Material_Result> resultdata = new List<SAPInventory.Material_Result>();
            resultdata.Add(new SAPInventory.Material_Result { Material = data.Material, Plant = data.Plant, WBSElementInternalID = data.WBSElementInternalID, StorageLocation = data.StorageLocation });
            return resultdata;
        }

        [HttpGet]
        public ActionResult Registration()
        {
            ViewBag.erp = Request.QueryString["ERP"];
            Session["ERPNAME"] = ViewBag.erp;
            return View();
        }

        [HttpPost]
        public ActionResult Registration(Registration registration)
        {
            var status = string.Empty;
            try
            {
                if (Session["ERPNAME"] == "SAP")
                {
                    var exist = db.Registrations.Any(x => x.ApiKey == registration.ApiKey) ? true : false;
                    if (!exist)
                    {
                        db.Registrations.Add(registration);
                        db.SaveChanges();
                        status = "Successfully registred.";
                        ModelState.Clear();
                    }
                }
                else if(Session["ERPNAME"].ToString() == "Sage")
                {

                    SageOneOAuth oAuth = new SageOneOAuth();
                    
                    if (oAuth.Token == "")
                        Response.Redirect(oAuth.AuthorizationURL);
                    else
                        Session["token"] = oAuth.Token;
                }
            }
            catch (Exception e)
            {
                status = e.Message;
            }
            ViewBag.status = status;

            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult PUSHDATA()
        {
            var inventroy_list = db.IF_Inventory.Where(x => x.Id == 0).ToList();
            return View(inventroy_list);
        }

        [HttpPost]
        public ActionResult PUSHDATA(IF_Inventory inventory)
        {
            Api_Intigartion.Models.Repository.SAPInventory objSAPInventory = new Api_Intigartion.Models.Repository.SAPInventory();
            var invfentories = Session["Data"] as List<SAPInventory.Result>;

            return View(objSAPInventory.AddUpdateSAPInventory(invfentories, inventory));
        }




    }
}